package com.classpath.streamsdemo.client;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

public class StreamsDemo {

    public static void main(String[] args) {
        final List<Integer> integers = List.of(1, 2, 3, 4, 5, 6, 7, 23, 45, 12, 56, 123, 456, 11, 56);

        Predicate<Integer> isEven = (val) -> val % 2 == 0;
        //Predicate<Integer> isOdd = (val) -> val % 2 == 0;

        integers.stream().filter(isEven).map(val -> "Even Number  : "+val).forEach(System.out::println);

        integers.stream().filter(isEven.negate()).map(val -> "Odd number: "+val).forEach(System.out::println);
    }


}